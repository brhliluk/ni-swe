# POPIS DATOVÝCH SAD

## 1. Electric Vehicle Population Data
Zdroj: https://catalog.data.gov/dataset/electric-vehicle-population-data
Formát: XML/JSON/**CSV**/RDF
Popis sady: Elektrická auta a plug-in hybridy momentálně zaregistrované ve státu Washington místním licenčním úřadem.
Popis vybraných dat:
    Registered cars
    --
    - VIN(1-10) - VIN číslo vozidla (prvních 10 čísel)
    - County - okres, ve kterém je vozidlo zaregistrováno
    - City - město, ve kterém je vozidlo zaregistrováno
    - State - stát, ve kterém je vozidlo zaregistrováno
    - ZIP Code - PSČ města, ve kterém je vozidlo zaregistrováno
    - Model Year - rok výroby modelu vozidla
    - Make - výrobce vozidla
    - Model - model vozidla
    - Electric Vehicle Type - typ elektrického vozidla (Plug-in hybrid/plně elektrické)
    - Electric range - dojezd pouze na elektřinu


## 2. Electric Vehicle by Year
Zdroj: https://data.world/jeffgswanson/electric-vehicle-by-year
Formát: CSV
Popis sady: Detailní popis elektrických vozidel
Popis vybraných dat:
    Electric cars
    --
    - automobile_id - id auta
    - model - model vozidla
    - manufacturer - výrobce vozidla
    - model_year - rok výroby modelu vozidla
    - city_range - dojezd po městě
    - highway_range - dojezd po dálnici
    - transmition_type - typ převodovky
    - engine_size - výkon motoru


## 3. WAOFM - Census - Population and Housing, 2000 and 2010
Zdroj: https://catalog.data.gov/dataset/waofm-census-population-and-housing-2000-and-2010
Formát: XML/JSON/**CSV**/RDF
Popis sady: Informace o obyvatelstvu a bydlení získané z desetiletého sčítání lidu podle veřejného zákona 94-171 o přerozdělování pro stát Washington za roky 2000 a 2010.
Popis vybraných dat:
    Population in Washington
    --
    - City Name - název města
    - Total Population 2000 - počet obyvatel v daném města v roce 2000
    - Total Population 2010 - počet obyvatel v daném města v roce 2010
    - Total Housing Units 2000 - počet ubytovacích jednotek v roce 2000
    - Total Housing Units 2010 - počet ubytovacích jednotek v roce 2010
    - Occupied Housing Units 2000 - počet obsazených ubytovacích jednotek v roce 2000
    - Occupied Housing Units 2010 - počet obsazených ubytovacích jednotek v roce 2010
    - Total Area (Square Miles) 2000 - plocha města v roce 2000 (míle čtvereční)
    - Total Area (Square Miles) 2010 - plocha města v roce 2010 (míle čtvereční)   
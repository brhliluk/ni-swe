Second checkpoint feedback

1. Jestliže tvoříte DSD v rámci TARQLu včetně komponent, nesmíte používat blank nody. Takto vám z každého řádku vstupní tabulky vznikají nové a nové komponenty v eg:dsd3, viz
```
qb:component  _:b243 .

_:b243  qb:dimension  eg:refArea ;
        qb:order      1 .

eg:dsd3  qb:component  _:b244 .

_:b244  qb:dimension  eg:refYear ;
        qb:order      2 .

eg:dsd3  qb:component  _:b245 .

_:b245  qb:measure  eg:areaTotal .

eg:dsd3  qb:component  _:b246 .
```
Kdybyste místo blank nodů používal IRI, tak budou vznikat stále stejné trojice, což nevadí. Ale u blank nodů je každý jiný, nový.

2. V DCAT katalogu máte `dct:language <https://id.loc.gov/vocabulary/iso639-2/eng> ;`, ale to neodpovídá DCAT-AP, kde je povinnost pro jazyk použít https://op.europa.eu/en/web/eu-vocabularies/dataset/-/resource?uri=http://publications.europa.eu/resource/dataset/language
3. v DCAT katalogu používáte "dcat:Distribution" jako predikát. Už z velkého písmena za : je vidět, že je to třída. Predikát je "dcat:distribution".
4. V RDFa vám chybí jazyk u textových literálů (je třeba mít v html použit atribut lang).
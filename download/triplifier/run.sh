#!/bin/bash

SCRIPT_DIR=$(dirname "$0")
DATA_DIR=$(realpath "$SCRIPT_DIR/../data/")
OUTPUT_DIR=$(realpath "$SCRIPT_DIR/../output/")
TARQL_BIN=${TARQL_BIN:-"../../../../TARQL/tarql-1.2/bin/tarql"}


# ---------------------------------------------------------------------------
# 1) Electric vehicles

$TARQL_BIN -v "${SCRIPT_DIR}/electric_cars.sparql" "${DATA_DIR}/electric_vehicles.csv" > "${OUTPUT_DIR}/electric-vehicles.ttl"


# ---------------------------------------------------------------------------
# 2) Electric vehicles population data

$TARQL_BIN -v "${SCRIPT_DIR}/electric_vehicle_population_data.sparql" "${DATA_DIR}/Electric_Vehicle_Population_Data.csv" > "${OUTPUT_DIR}/electric-vehicles-population-data.ttl"


# ---------------------------------------------------------------------------
# 3) Population and housing

$TARQL_BIN -v "${SCRIPT_DIR}/WAOFM_Population_and_housing.sparql" "${DATA_DIR}/WAOFM_Population_and_Housing_2000_and_2010.csv" > "${OUTPUT_DIR}/waofm-population-and-housing.ttl"